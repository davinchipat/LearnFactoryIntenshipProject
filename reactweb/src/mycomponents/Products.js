import React, {Component} from 'react';
export default class products extends Component{
    render(){
        const {product} = this.props;
        const products = product.map(prod =>{
            return(
                <div>
                    <p>{prod._id} {prod.name} </p>
                </div>
            )
        })
        return(
            <div>
                <h2>single product page.</h2>
                {products}
            </div>
        )
    }
}