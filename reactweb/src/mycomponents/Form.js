import React, {Component} from 'react';
export default class Form extends Component{
    state = {
        name: '',
        street: '',
        city: '',
        state: '',
        phone: '',
    }

    handleChange = (e) =>{
        const fields = this.state;
        fields[e.target.name] = e.target.value;
        this.setState(fields);
        //console.log(fields)
    }

    handleSubmit =(e) =>{
        e.preventDefault();
        const data = {
            name: this.state.name,
            street: this.state.street,
            city: this.state.city,
            state: this.state.state,
            phone: this.state.phone
        }
        this.props.handleData(data);
        this.setState({
            name: " ",
            street: " ",
            city: " ",
            state: " ",
            phone: " "
        })
    }
        
    render(){
        return(
            <div>
                <h2>User Data Collection </h2>
                <form onSubmit = {this.handleSubmit}>
                    <div>
                        <input type='text' name='name' value = {this.state.name} onChange = {this.handleChange}/>
                    </div>
                    <div>
                    <input type='text' name='street' value = {this.state.street} onChange = {this.handleChange}/>
                    </div>
                    <div>
                    <input type='text' name='city' value = {this.state.city} onChange = {this.handleChange}/>
                    </div>
                    <div>
                    <input type='text' name='state' value = {this.state.state} onChange = {this.handleChange}/>
                    </div>
                    <div>
                    <input type='text' name='phone' value = {this.state.phone} onChange = {this.handleChange}/>
                    </div>
                    <button type = 'submit'>Submit</button>
                    
                </form>
            </div>
        )
    }
}