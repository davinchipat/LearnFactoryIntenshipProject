import React, {Component} from 'react';

export default class ToggleButton extends Component{

    state={//anything js is written here

        clicked:false

    }

    handleClick = ()=>{

        this.setState({clicked:!this.state.clicked});//this.setState is used to set state//for assigning and reassigning state. this re[p the TOggleButton

    }

    handleButton =() =>{

        if(this.state.clicked){

            return ( 

                <button  style={styles.button}  onClick = {this.handleClick}>Logout</button> //inline styling  style ={{color: 'red'}}

            );

        }else{

            return  <button  style = {styles.button}  onClick = {this.handleClick}>Login</button>//inline styling style ={{color: 'green'}}

        }

    }

        render(){

            return(

                <div style = {styles.body}>

                    {this.handleButton()}

                    </div>

            )

        }

}



const styles = {

    body:{

        background:'gray',

        color     :'black',

        width: '200px'

    },

    button:{

            background:'green',

            color:'black',

            fontSize: '18px'

        }

      

    }