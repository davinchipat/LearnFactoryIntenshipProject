import React, {Component} from 'react';
import Form from './Form'
export default class Profile extends Component{
    state = {
        name: '',
        state: '',
        city: '',
        street: '',
        phone: '',

    }
    handleData = (data) =>{
        console.log(data);
        this.setState({
            name:data.name,
            street:data.street,
            city:data.city,
            state:data.state,
            phone: data.phone
        })
    }
    render(){
        return(
          <div>
              <img id='prof'src = {require('../img/bently.jpg' )} />
              <p><strong>Name:</strong>{this.state.name}</p>
              <p><strong>State:</strong>{this.state.state}</p>
              <p><strong>City:</strong>{this.state.city}</p>
              <p><strong>Street:</strong>{this.state.street}</p>
              <p><strong>phone:</strong>{this.state.phone}</p>
              <Form handleData = {this.handleData} />
          </div>  
        );
    }
}