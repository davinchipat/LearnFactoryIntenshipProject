import React, { Component } from 'react';
import {Switch, Route} from 'react-router-dom'
import Home from '../mycomponents/Home';
import ProductList from '../mycomponents/ProductList'
import Product from './Product'
import My404 from './My404'
import contents from './Data'
//import logo from '../logo.svg';
//import '../styles/App.css';
// import Header from './Header'
// import Logo from './Logo'
// import Headerwriteup from './Headerwriteup'
// import Writehead from './Writehead'
// import Anotherone from './Anotherone'
// import Profile from './Profile'
// import ToggleButton from './ToggleButton'

class App extends Component {
  state ={
    comments: []
  }

  componentDidMount(){
    const comments = this.state.comments;
    comments.push(contents);
    this.setState({comments:comments});
  }
  render() {
    const {comments} = this.state
    return (
      <main>
        <Switch>
          <Route path='/home' component = {Home}/>
          <Route path='/ProductList' component= {ProductList} />
          <Route path='/productId' component={(props) => <Product data = {comments} {...props} />} />
          <Route path='/*' component={My404} />
        </Switch>
      </main>
      // <div className="App">
      //   <header className="App-header">
      //     <Header  />
      //     <Logo />
      //     <Headerwriteup />
      //     <Writehead />
      //     <Logo />
      //   </header>
      //   <ToggleButton />
      //   <Anotherone  />
      //   <Profile />
        
      // </div>
    );
  }
}

export default App;
