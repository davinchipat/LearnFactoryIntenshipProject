import React, {Component} from 'react'
import {Link} from 'react-router-dom';
const My404 = () => {
    return(
        <div>
            <h1>404 page not Found!</h1>
            <Link to='./home'>Home</Link>
        </div>
    )
}
export default My404;