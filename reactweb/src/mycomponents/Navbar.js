import React, {Component} from 'react';
export default class Navbar extends Component{
    render() {
        return (
            <div id='navs'>
            
                <ul>
                    <li>Home</li>
                    <li>Contact</li>
                    <li>About Us</li>
                    <h3 className = "tagline">{this.props.tagline}</h3>
                </ul>
            </div>
        )
    }
} 