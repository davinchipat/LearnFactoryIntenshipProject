import React, {Component} from 'react'
import Products from './Products'
import {Link} from 'react-router-dom';
export default class Product extends Component{
    render(){
        const {data, match} = this.props;
        console.log(match);
        const allProducts = data.map(product =>( <Products key={product._id} product={product} />))
        return(
            <div>
                <Link to='./home'>Go to Homepage</Link>
                <Link to='./ProductList'>Go to ProductList</Link>
                {allProducts}
            
            </div>
        )
    }
}